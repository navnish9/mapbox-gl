import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
// import { viteExternalsPlugin } from "vite-plugin-externals";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    // viteExternalsPlugin({
    //   threebox:
    //     "https://cdn.jsdelivr.net/gh/jscastro76/threebox@v.2.2.2/dist/threebox.min.js",
    // }),
  ],
});
