import { useRef, useEffect, useState } from "react";
import mapboxgl, { Map } from "mapbox-gl";

import "./App.css";

mapboxgl.accessToken =
  "pk.eyJ1IjoibmF2bmlzaDkiLCJhIjoiY2xraHNzeXpwMDJ5NjNlb2M0d204MGd5aSJ9.e39JLv9wvwKWKk-VtEycJw";

export default function App() {
  const mapContainer = useRef<HTMLDivElement>(null);
  const mapRef = useRef<Map>();
  const [lng, setLng] = useState(-73.97627);
  const [lat, setLat] = useState(40.75155);
  const [zoom, setZoom] = useState(15.4);

  useEffect(() => {
    if (mapRef.current || !mapContainer.current) return; // initialize map only once
    const mapInstance = new mapboxgl.Map({
      container: mapContainer.current as HTMLDivElement,
      style: "mapbox://styles/mapbox/streets-v12",
      center: [lng, lat],
      zoom: zoom,
      pitch: 64.9,
      bearing: 172.5,
      antialias: true,
    });
    mapRef.current = mapInstance;
  });

  useEffect(() => {
    if (mapRef.current) {
      mapRef.current.on("move", () => {
        if (mapRef.current) {
          setLng(+mapRef.current.getCenter().lng.toFixed(4));
          setLat(+mapRef.current.getCenter().lat.toFixed(4));
          setZoom(+mapRef.current.getZoom().toFixed(2));
        }
      });

      const tb = (window.tb = new Threebox(
        mapRef.current,
        mapRef.current.getCanvas().getContext("webgl"),
        {
          defaultLights: true,
        }
      ));

      mapRef.current.on("style.load", () => {
        mapRef.current?.addLayer({
          id: "custom-threebox-model",
          type: "custom",
          renderingMode: "3d",
          onAdd: function () {
            // Creative Commons License attribution:  Metlife Building model by https://sketchfab.com/NanoRay
            // https://sketchfab.com/3d-models/metlife-building-32d3a4a1810a4d64abb9547bb661f7f3
            const scale = 3.2;
            const modelOptions1 = {
              obj: "https://docs.mapbox.com/mapbox-gl-js/assets/metlife-building.gltf",
              type: "gltf",
              scale: { x: scale, y: scale, z: 2.7 },
              units: "meters",
              rotation: { x: 90, y: -90, z: 0 },
            };

            const modelOptions2 = {
              obj: "https://docs.mapbox.com/mapbox-gl-js/assets/34M_17/34M_17.gltf",
              type: "gltf",
              scale: { x: scale, y: scale, z: 2.7 },
              units: "meters",
              rotation: { x: 90, y: -90, z: 0 },
            };

            tb.loadObj(modelOptions1, (model) => {
              model.setCoords([-73.976799, 40.754145]);
              model.setRotation({ x: 0, y: 0, z: 241 });
              tb.add(model);
            });

            tb.loadObj(modelOptions2, (model) => {
              model.setCoords([-73.9774, 40.7532]);
              model.setRotation({ x: 0, y: 0, z: 241 });
              tb.add(model);
            });
          },

          render: function () {
            tb.update();
          },
        });
      });
    } // wait for map to initialize
  }, []);

  return (
    <>
      <div className="sidebar">
        Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
      </div>
      <div ref={mapContainer} className="map-container" />
    </>
  );
}
